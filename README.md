# Watchtower

Interactive film production management tool. Watchtower allows you to see the big picture of a 
short film or episode and unpack as much information as needed, down to the duration of a shot and
assets used in it. All in the space of one screen. Read the announcement on 
[studio.blender.org](https://studio.blender.org/blog/introducing-watchtower/).

## Features

* Grid view (for shots and assets) with grouping and filtering tools
* Detail view
* Timeline showing individual shots as well as task statuses
* Production tracking tool agnostic (fetch data once, store as static JSON files)
* Read-only, no-auth

## Setup Guide

You can deploy Watchtower in your pipeline by following the instructions in `pipeline/README.md`.

## Development Guide

Contributions to Watchtower are welcome! Check out `docs/develop.md` for info on how to get
started.


